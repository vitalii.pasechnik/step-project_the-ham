"use strict";

import { services } from "./serviseInfo.js";

export const TABS = () => {

    const tabsWrapper = document.body.querySelector('.service-menu__wrapper');
    const descriptionWrapper = document.body.querySelector('.service__content');

    loadTabs();
    loadTabsDescription();

    const tabs = document.body.querySelector('.service__menu-list');
    tabs.addEventListener('mousedown', (e) => showTabContent(e));

    function loadTabs() {
        const list = document.createElement('ul');
        list.classList.add('service__menu-list', 'nav-menu');
        services.forEach(service => {
            list.insertAdjacentHTML('beforeend', `<li class="service__item" data-category="${service.name}">${service.name}</li>`);
        })
        tabsWrapper.append(list);
        const firstService = document.body.querySelector('.service__item');
        firstService.classList.add('active');
    }

    function loadTabsDescription(ind = 0) {
        descriptionWrapper.innerHTML = `
                <div class="service__item-description">
                    <div class="service-item__image-wrapper">
                        <img src="${services[ind].img}" alt="service.name-img" class="service__item-img">
                    </div>
                    <p class="service__item-text">${services[ind].description}</p>
                </div>`;
    }

    const showTabContent = (e) => {
        [...tabs.children].forEach(tab => {
            if (tab === e.target && !tab.classList.contains('active')) {
                tab.classList.add("active");
                const tabCategory = tab.dataset['category'];
                services.forEach((service, i) => {
                    if (service.name === tabCategory) {
                        loadTabsDescription(i);
                        const p = descriptionWrapper.querySelector('p');
                        const tabText = service.description;
                        p.innerText = '';
                        tabText.split('').forEach((letter, i) => setTimeout(() => p.append(letter), 1 * i));
                    }
                });
            } else if (tab !== e.target) tab.classList.remove("active");
        });
    }

}